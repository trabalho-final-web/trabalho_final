Tecnologia em Análise e Desenvolvimento de Sistemas
Setor de Educação Profissional e Tecnológica - SEPT
Universidade Federal do Paraná - UFPR

---

DS122 - Desenvolvimento Web 1
Prof. Alexander Robert Kutzke

# Trabalho Final

Esse trabalho tem como objetivo apresentar um blog de tecnologia com a finalidade de demonstrar o conteúdo aprendido na disciplina DS122.

Participantes:
- Guilherme Roncon 
- João Ramos Neto
- Luigi Girardi 

## Defesa e conceitos

O conceito do site trata-se de um blog sobre tecnologia geral e utlimas notícias do mundo tech. Um usuário administrador tem a capacidade de editar e adicionar posts e categorias e criar novos usuarios.

> Tecnologias Utilizadas

- HTML
- CSS
- Bootstrap
- PHP
- MySQL
- JavaScript


Para que o projeto funcione corretamente são necessários os seguintes requisitos:

1. Fazer o clone deste repositório em sua máquina local
2. Ativar seu servidor Web
3. Configurar as credenciais do banco de dados no arquivo include/db.php
4. Rodar o arquivo include/createDB.php para fazer os inserts nas tabelas de categoria e o do usuário admin
5. Abrir o navegador e acessar o endereço incial index.php
6. Para ter acesso a página de admin utilize o email: adm@adm.com e a senha: 123

## Funcionalidades e implementação

- Vizualização dos posts
- Comentarios de usuarios logados e nao logados
- Categorias responsivas ao banco de dados 
- Página do admin
- Cadastro e login dos usuários (sistema de autentificação)
- Adição, edição e remoção dos posts
- Adição, edição e remoção das categorias
- Aprovação ou desaprovação dos comentarios
