<?php
// session_start(); 
include "db.php";
require 'vendor/autoload.php';

use DebugBar\StandardDebugBar;

?>
<div class="col-md-4">

    <!-- Blog Search Well -->
    <div class="well">
        <h4>Procurar no blog</h4>
        <form action="search.php" method="post">
            <div class="input-group">
                <input name="search" type="text" class="form-control">
                <span class="input-group-btn">
                    <button name="submit" class="btn btn-default" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
        </form> <!-- search form -->
        <!-- /.input-group -->
    </div>

    <!-- Login -->
    <div class="well">

        <?php

        // echo $_SESSION['user_role'];

         if (isset($_SESSION['user_role'])) : ?>
            
            <h4>Logado como <?php echo $_SESSION['username'] ?></h4>

        <?php else : ?>

            <h4>Login</h4>
            <!-- <h5>Wrong Password</h5> -->
            <form action="login.php" method="post">
                <div class="form-group">
                    <input name="username" type="text" class="form-control" placeholder="Nome de usuário">

                </div>

                <div class="from-group">
                    <input name="password" type="password" class="form-control" placeholder="Senha">
                    <span class="btn">
                        <button class="btn btn-primary" name="login" type="submit">Logar</button>

                    </span>
                </div>

            </form>

        <?php endif; ?>



        <!-- <h4>Login</h4>
    <form action="include/login.php" method="post">
    <div class="form-group">
        <input name="username" type="text" class="form-control" placeholder="Enter Username">

    </div>

    <div class="from-group">
        <input name="password" type="password" class="form-control" placeholder="Enter Password">
        <span class="input-group-btn">
            <button class="btn btn-primary" name="login" type="submit">Submit</button>

        </span>
    </div>
    
    </form> -->
        <!-- search form -->
        <!-- /.input-group -->
    </div>





    <!-- Blog Categories Well -->
    <div class="well">
        <?php
        $query = "SELECT * FROM categories";
        $select_cat_sidebar = mysqli_query($connection, $query);


        ?>

        <h4>Categorias</h4>
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled">
                    <?php
                    while ($row = mysqli_fetch_assoc($select_cat_sidebar)) {
                        $cat_title = $row['cat_title'];
                        $cat_id = $row['cat_id'];
                        // dd($cat_id);

                        echo "<li><a href='category.php?category=$cat_id'>{$cat_title}</a></li>";
                    }

                    ?>

                </ul>
            </div>

            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>

    <!-- Side Widget Well -->
    <?php include "widget.php"; ?>
</div>