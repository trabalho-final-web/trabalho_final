<?php
include "db.php";
include "insert_bd/insert_user.php";
include "insert_bd/insert_cat.php";

// sql to create table
$table1 = "CREATE TABLE users (
    user_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(30) NOT NULL,
    user_password VARCHAR(30) NOT NULL,
    user_firstname VARCHAR(30) NOT NULL,
    user_lastname VARCHAR(30) NOT NULL,
    user_email VARCHAR(50) NOT NULL,
    user_role VARCHAR(15) NOT NULL,
    user_reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";

$table2 = "CREATE TABLE post (
      post_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      post_category_id INT(6) NOT NULL,
      post_title VARCHAR(255) NOT NULL,
      post_author VARCHAR(50),
      post_date date,
      post_image TEXT,
      post_content TEXT,
      post_tag VARCHAR(255),
      post_status VARCHAR(30)
)";

$table3 = "CREATE TABLE comments(
      comm_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      comm_post_id INT(6) NOT NULL,
      comm_date date,
      comm_author VARCHAR(50),
      comm_email VARCHAR(50) NOT NULL,
      comm_status VARCHAR(30)
)";
$table4 = "CREATE TABLE categories(
      cat_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      cat_title VARCHAR(255) NOT NULL
)";



$tables = [$table1,$table2,$table3,$table4];
$errors = [];

foreach ($tables as $k => $sql) {
    $query = @$connection->query($sql);

    if(!$query)
       $errors[] = "Table $k : Creation failed ($connection->error)";
    else
       $errors[] = "Table $k : Creation done";
}


foreach($errors as $msg) {
   echo "$msg <br>";
}

$connection->close();
?>